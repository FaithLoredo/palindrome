
public class Palindrome {
	static boolean isPalindrome(String input) {
		input.toUpperCase().replaceAll(" ", "");
		int i = 0, j = input.length() - 1;
		while (i < j) {
			if (input.charAt(i) != input.charAt(j)) 
                return false; 
			i++; 
            j--; 
		}
		return true;
	}
	
	public static void main(String[] args) {
		System.out.println("Is Anna a palindrome? "+Palindrome.isPalindrome("anna"));
		System.out.println("Is Race Car a palindrome? "+Palindrome.isPalindrome("Race Car"));
		System.out.println("Is yolo a palindrome? "+Palindrome.isPalindrome("yolo"));
	}
}
