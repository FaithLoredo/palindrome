

import static org.junit.Assert.*;

import org.junit.Test;

public class PalindromeTest {

	@Test
	public final void testIsPalindrome() {
		assertTrue("Invalid input", Palindrome.isPalindrome("anna"));
	}
	@Test
	public final void testFailPalindrome() {
		assertFalse("Invalid input", Palindrome.isPalindrome("Lmao"));
	}
	@Test
	public final void testMaybePalindrome() {
		assertTrue("Invalid input", Palindrome.isPalindrome("A"));
	}

	@Test
	public final void testNotQuitePalindrome() {
		assertFalse("Invalid input", Palindrome.isPalindrome("Annna"));
	}
}
